package objetos;

import modelos.Objeto;

/**
 * Clase Pocion que hereda de Objeto
 * @author DIEGO LITTLELION
 *
 */
public class Pocion extends Objeto{
	private int cantidadCuracion;
	
	/**
	 * Constructor de la clase Pocion
	 * @param objeto String con el nombre de la poción
	 * @param cantidadCuracion int con la cantidad de curación de la poción
	 */
	public Pocion(String objeto, int cantidadCuracion) {
		super(objeto);
		this.cantidadCuracion = cantidadCuracion;
	}

	/**
	 * Devuelve la cantidad de curación de una poción
	 * @return int con la cantidad de curación
	 */
	public int getCantidadCuracion() {
		return cantidadCuracion;
	}

	/**
	 * Establece la cantidad de curación
	 * @param cantidadCuracion int con la cantidad de curación
	 */
	public void setCantidadCuracion(int cantidadCuracion) {
		this.cantidadCuracion = cantidadCuracion;
	}
}
