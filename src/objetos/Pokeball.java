package objetos;

import modelos.Objeto;

/**
 * Clase Pokeball que hereda de Objeto
 * @author DIEGO LITTLELION
 *
 */
public class Pokeball extends Objeto {
	private int probabilidadCaptura;
	
	/**
	 * Constructor de la clase Pokeball
	 * @param objeto String con el nombre de la pokeball
	 * @param probabilidad int con la probabilidad de captura de la pokeball
	 */
	public Pokeball(String objeto, int probabilidad) {
		super(objeto);
		this.probabilidadCaptura = probabilidad;
	}

	/**
	 * Devuelve la probabilidad de captura de la pokeball
	 * @return int con la probabilidad de captura
	 */
	public int getProbabilidadCaptura() {
		return probabilidadCaptura;
	}

	/**
	 * Establece la probabilidad de captura
	 * @param probabilidadCaptura int con la probabilidad de captura
	 */
	public void setProbabilidadCaptura(int probabilidadCaptura) {
		this.probabilidadCaptura = probabilidadCaptura;
	}
}
