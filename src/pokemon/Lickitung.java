package pokemon;

import ataques.*;
import conexion.Json;
import modelos.Pokemon;
import modelos.PokemonBasic;

/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Lickitung extends Pokemon {
	private Lenguetazo lenguetazo = new Lenguetazo();
	private Pisoton pisoton = new Pisoton();
	private Latigazo latigazo = new Latigazo();
	private Portazo portazo = new Portazo();
	private PokemonBasic base = getBase();

	/**
	 * Genera a Lickitung
	 * 
	 * @param nombre - nombre del pokemon elegido por el usuario
	 * @param nivel  - nivel del pokemon generado aleatoriamente
	 */
	public Lickitung(int id, String nombre, int nivel) {
		this.id = id;
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo = "Normal";
		this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
		this.psOriginal = this.ps;
		this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
		this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
		this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
		this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
		this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
		this.ataques[0] = lenguetazo;
		this.ataques[1] = pisoton;
		this.ataques[2] = latigazo;
		this.ataques[3] = portazo;
	}

	public PokemonBasic getBase() {
		Json json = new Json("assets/json/Lickitung.json");
		PokemonBasic base = json.getEstadisticas();
		return base;
	}
}
