package pokemon;

import ataques.*;
import conexion.Json;
import modelos.Pokemon;
import modelos.PokemonBasic;

/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Caterpie extends Pokemon {
	private Placaje placaje = new Placaje();
	private DisparoDemora disparo = new DisparoDemora();
	private NoAtaque noataque = new NoAtaque();
	private Picadura picadura = new Picadura();
	private PokemonBasic base = getBase();

	/**
	 * Genera a Caterpie
	 * 
	 * @param nombre - nombre del pokemon elegido por el usuario
	 * @param nivel  - nivel del pokemon generado aleatoriamente
	 */
	public Caterpie(int id, String nombre, int nivel) {
		this.id = id;
		this.base = getBase();
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo = "Bicho";
		this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
		this.psOriginal = this.ps;
		this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
		this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
		this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
		this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
		this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
		this.ataques[0] = placaje;
		this.ataques[1] = disparo;
		this.ataques[2] = picadura;
		this.ataques[3] = noataque;
	}

	/**
	 * Devuelve las estadísticas leídas de un Json
	 * 
	 * @return base - estadísticas base del pokemon
	 */
	public PokemonBasic getBase() {
		Json json = new Json("assets/json/Caterpie.json");
		PokemonBasic base = json.getEstadisticas();
		return base;
	}
}
