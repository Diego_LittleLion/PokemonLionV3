package pokemon;

import ataques.*;
import conexion.Json;
import modelos.Pokemon;
import modelos.PokemonBasic;

/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Squirtle extends Pokemon {
	private Placaje placaje = new Placaje();
	private Burbuja burbuja = new Burbuja();
	private PistolaAgua pistola = new PistolaAgua();
	private Hidrobomba hidrobomba = new Hidrobomba();
	private PokemonBasic base = getBase();

	/**
	 * Genera a Squirtle
	 * 
	 * @param nombre - nombre del pokemon elegido por el usuario
	 * @param nivel  - nivel del pokemon generado aleatoriamente
	 */
	public Squirtle(int id, String nombre, int nivel) {
		this.id = id;
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo = "Agua";
		this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
		this.psOriginal = this.ps;
		this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
		this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
		this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
		this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
		this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
		this.ataques[0] = placaje;
		this.ataques[1] = burbuja;
		this.ataques[2] = pistola;
		this.ataques[3] = hidrobomba;
	}

	/**
	 * Devuelve las estadísticas leídas de un Json
	 * 
	 * @return base - estadísticas base del pokemon
	 */
	public PokemonBasic getBase() {
		Json json = new Json("assets/json/Squirtle.json");
		PokemonBasic base = json.getEstadisticas();
		return base;
	}
}
