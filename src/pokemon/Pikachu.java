package pokemon;

import ataques.*;
import conexion.Json;
import modelos.Pokemon;
import modelos.PokemonBasic;

/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Pikachu extends Pokemon {
	private Impactrueno impactrueno = new Impactrueno();
	private AtaqueRapido aRapido = new AtaqueRapido();
	private Trueno trueno = new Trueno();
	private Chispa chispa = new Chispa();
	private PokemonBasic base = getBase();

	/**
	 * Genera a Pikachu
	 * 
	 * @param nombre - nombre del pokemon elegido por el usuario
	 * @param nivel  - nivel del pokemon generado aleatoriamente
	 */
	public Pikachu(int id, String nombre, int nivel) {
		this.id = id;
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo = "Electrico";
		this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
		this.psOriginal = this.ps;
		this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
		this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
		this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
		this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
		this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
		this.ataques[0] = impactrueno;
		this.ataques[1] = aRapido;
		this.ataques[2] = trueno;
		this.ataques[3] = chispa;
	}

	/**
	 * Devuelve las estadísticas leídas de un Json
	 * 
	 * @return base - estadísticas base del pokemon
	 */
	public PokemonBasic getBase() {
		Json json = new Json("assets/json/Pikachu.json");
		PokemonBasic base = json.getEstadisticas();
		return base;
	}
}