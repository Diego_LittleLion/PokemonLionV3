package pokemon;

import ataques.*;
import conexion.Json;
import modelos.Pokemon;
import modelos.PokemonBasic;

/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Enemigo extends Pokemon {
	private Lenguetazo lenguetazo = new Lenguetazo();
	private Pisoton pisoton = new Pisoton();
	private Latigazo latigazo = new Latigazo();
	private Portazo portazo = new Portazo();
	private Placaje placaje = new Placaje();
	private Burbuja burbuja = new Burbuja();
	private PistolaAgua pistola = new PistolaAgua();
	private Hidrobomba hidrobomba = new Hidrobomba();
	private PulsoDragon pulso = new PulsoDragon();
	private TajoAereo tajo = new TajoAereo();
	private Triturar triturar = new Triturar();
	private Hiperrayo hiperrayo = new Hiperrayo();
	private Impactrueno impactrueno = new Impactrueno();
	private AtaqueRapido aRapido = new AtaqueRapido();
	private Trueno trueno = new Trueno();
	private Chispa chispa = new Chispa();
	private Vendaval vendaval = new Vendaval();
	private DisparoDemora disparo = new DisparoDemora();
	private NoAtaque noataque = new NoAtaque();
	private Picadura picadura = new Picadura();
	private PokemonBasic base;

	/**
	 * Genera al enemigo
	 * 
	 * @param especie - especie del pokemon generado aleatoriamente
	 * @param nivel   - nivel del pokemon generado aleatoriamente
	 */
	public Enemigo(String especie, int nivel) {
		this.nombre = especie;
		this.nivel = nivel;
		switch (especie) {
		case "Caterpie":
			base = getBase(especie);
			this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
			this.psOriginal = this.ps;
			this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
			this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
			this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
			this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
			this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
			this.tipo = "Bicho";
			this.ataques[0] = placaje;
			this.ataques[1] = disparo;
			this.ataques[2] = picadura;
			this.ataques[3] = noataque;
			break;
		case "Lickitung":
			base = getBase(especie);
			this.tipo = "Normal";
			this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
			this.psOriginal = this.ps;
			this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
			this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
			this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
			this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
			this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
			this.ataques[0] = lenguetazo;
			this.ataques[1] = pisoton;
			this.ataques[2] = latigazo;
			this.ataques[3] = portazo;
			break;
		case "Pidgey":
			base = getBase(especie);
			this.tipo = "Volador Normal";
			this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
			this.psOriginal = this.ps;
			this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
			this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
			this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
			this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
			this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
			this.ataques[0] = placaje;
			this.ataques[1] = aRapido;
			this.ataques[2] = tajo;
			this.ataques[3] = vendaval;
			break;
		case "Pikachu":
			base = getBase(especie);
			this.tipo = "Electrico";
			this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
			this.psOriginal = this.ps;
			this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
			this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
			this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
			this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
			this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
			this.ataques[0] = impactrueno;
			this.ataques[1] = aRapido;
			this.ataques[2] = trueno;
			this.ataques[3] = chispa;
			break;
		case "Squirtle":
			base = getBase(especie);
			this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
			this.psOriginal = this.ps;
			this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
			this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
			this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
			this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
			this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
			this.tipo = "Agua";
			this.ataques[0] = placaje;
			this.ataques[1] = burbuja;
			this.ataques[2] = pistola;
			this.ataques[3] = hidrobomba;
			break;
		case "Rayquaza":
			base = getBase(especie);
			this.ps = ((2 * base.getPs() + 100 )* nivel) / 100 + 10;
			this.psOriginal = this.ps;
			this.ataque = (2 * base.getAtaque() * nivel) / 100 + 5;
			this.defensa = (2 * base.getDefensa() * nivel) / 100 + 5;
			this.ataqueEspecial = (2 * base.getAtaqueEspecial() * nivel) / 100 + 5;
			this.defensaEspecial = (2 * base.getDefensaEspecial() * nivel) / 100 + 5;
			this.velocidad = (2 * base.getVelocidad() * nivel) / 100 + 5;
			this.tipo = "Volador Dragón";
			this.ataques[0] = pulso;
			this.ataques[1] = tajo;
			this.ataques[2] = triturar;
			this.ataques[3] = hiperrayo;
			break;
		}
	}

	/**
	 * Devuelve las estadística leídas de un Json
	 * 
	 * @param pokemon - especie de pokemon que queremos leer
	 * @return base - estadísticas base de dicho pokemon
	 */
	public PokemonBasic getBase(String pokemon) {
		Json json = new Json("assets/json/" + pokemon + ".json");
		PokemonBasic base = json.getEstadisticas();
		return base;
	}
}
