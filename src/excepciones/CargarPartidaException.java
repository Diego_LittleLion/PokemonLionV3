package excepciones;
/**
 * Excepción para capturar errores al cargar la partida
 * 
 * @author tetem Cuando esté en modo interfaz será un pantallazo azul
 */
@SuppressWarnings("serial")
public class CargarPartidaException extends Exception{
}
