package ataques;

/**
 * Clase padre para los ataques
 * @author DIEGO LITTLELION
 *
 */
public abstract class Ataque {
	protected String nombre;
	protected String categoria;
	protected String tipo;
	protected int pp;
	protected int ppOriginal;
	protected int potencia;
	
	/**
	 * Devuelve el pp de un pokemon
	 * @return int
	 */
	public int getPp() {
		return pp;
	}
	
	/**
	 * Devuelve el pp de un pokemon
	 * @return int
	 */
	public int getPpOriginal() {
		return ppOriginal;
	}
	
	/**
	 * Establece la cantidad de pp de un pokemon
	 */
	public void setPp(int pp) {
		this.pp = pp;
	}
	
	/**
	 * Devuelve el nombre de un pokemon
	 * @return string
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Devuelve la categoria de un pokemon
	 * @return string
	 */
	public String getCategoria() {
		return categoria;
	}
	
	/**
	 * Devuelve el tipo de un pokemon
	 * @return string
	 */
	public String getTipo() {
		return tipo;
	}
	
	/**
	 * Devuelve la potencia de un pokemon
	 * @return int
	 */
	public int getPotencia() {
		return potencia;
	}
}
