package ataques;

public class Portazo extends Ataque{
	public Portazo() {
		this.nombre = "Portazo";
		this.categoria = "Fisico";
		this.tipo = "Normal";
		this.pp = 20;
		this.ppOriginal = 20;
		this.potencia = 80;
	}
}
