package ataques;

public class Burbuja extends Ataque{
	public Burbuja() {
		this.nombre = "Burbuja";
		this.categoria = "Especial";
		this.tipo = "Agua";
		this.pp = 30;
		this.ppOriginal = 30;
		this.potencia = 40;
	}
}
