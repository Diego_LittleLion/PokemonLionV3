package ataques;

public class Pisoton extends Ataque{
	public Pisoton() {
		this.nombre = "Pisoton";
		this.categoria = "Fisico";
		this.tipo = "Normal";
		this.pp = 20;
		this.ppOriginal = 20;
		this.potencia = 65;
	}
}
