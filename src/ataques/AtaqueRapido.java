package ataques;

public class AtaqueRapido extends Ataque{
	public AtaqueRapido() {
		this.nombre = "Ataque Rapido";
		this.categoria = "Fisico";
		this.tipo = "Normal";
		this.pp = 30;
		this.ppOriginal = 30;
		this.potencia = 40;
	}
}
