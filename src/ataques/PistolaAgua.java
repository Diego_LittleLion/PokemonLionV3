package ataques;

public class PistolaAgua extends Ataque{
	public PistolaAgua() {
		this.nombre = "Pistola Agua";
		this.categoria = "Especial";
		this.tipo = "Agua";
		this.pp = 25;
		this.ppOriginal = 25;
		this.potencia = 40;
	}
}
