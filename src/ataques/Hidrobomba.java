package ataques;

public class Hidrobomba extends Ataque{
	public Hidrobomba() {
		this.nombre = "Hidrobomba";
		this.categoria = "Especial";
		this.tipo = "Agua";
		this.pp = 5;
		this.ppOriginal = 5;
		this.potencia = 110;
	}
}
