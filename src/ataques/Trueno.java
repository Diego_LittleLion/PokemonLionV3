package ataques;

public class Trueno extends Ataque{
	public Trueno() {
		this.nombre = "Trueno";
		this.categoria = "Especial";
		this.tipo = "Electrico";
		this.pp = 10;
		this.ppOriginal = 10;
		this.potencia = 110;
	}
}