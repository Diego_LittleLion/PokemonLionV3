package ataques;

public class Chispa extends Ataque{
	public Chispa() {
		this.nombre = "Chispa";
		this.categoria = "Especial";
		this.tipo = "Electrico";
		this.pp = 20;
		this.ppOriginal = 20;
		this.potencia = 65;
	}
}