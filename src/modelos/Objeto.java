package modelos;

/**
 * Clase abstracta objeto
 * @author DIEGO LITTLELION
 *
 */
public abstract class Objeto {
	protected String nombre;

	/**
	 * Establece el nombre del Objeto
	 * @param objeto String con el nombre del objeto
	 */
	public Objeto(String objeto) {
		this.nombre = objeto;
	}

	/**
	 * Devuelve el nombre del Objeto
	 * @return String con el nombre del objeto
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Establece el nombre del objeto
	 * @param objeto String con el nombre del objeto
	 */
	public void setNombre(String objeto) {
		this.nombre = objeto;
	}
}

