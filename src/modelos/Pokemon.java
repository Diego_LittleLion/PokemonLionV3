package modelos;

import java.sql.Timestamp;
import java.util.Date;

import ataques.Ataque;

/**
 * 
 * @author DIEGO LITTLELION
 *
 */
public abstract class Pokemon {
	protected int id;
	protected String nombre;
	protected int nivel;
	protected String tipo;
	protected Ataque[] ataques = new Ataque[4];
	protected int ps;
	protected int psOriginal;
	protected int ataque;
	protected int defensa;
	protected int ataqueEspecial;
	protected int defensaEspecial;
	protected int velocidad;
	protected Timestamp momentoCaptura = new Timestamp(new Date().getTime());

	/**
	 * Devuelve el ataque especial del pokemon
 	 * @return int con el ataque especial
	 */
	public int getAtaqueEspecial() {
		return ataqueEspecial;
	}

	/**
	 * Establece el ataque especial de un pokemon
	 * @param ataqueEspecial int con el ataque especial
	 */
	public void setAtaqueEspecial(int ataqueEspecial) {
		this.ataqueEspecial = ataqueEspecial;
	}

	/**
	 * Devuelve la defensa especial de un pokemon
	 * @return int con la defensa especial
	 */
	public int getDefensaEspecial() {
		return defensaEspecial;
	}

	/**
	 * Establece la defensa especial de un pokemon
	 * @param defensaEspecial int con la defensa especial de un pokemon
	 */
	public void setDefensaEspecial(int defensaEspecial) {
		this.defensaEspecial = defensaEspecial;
	}

	/**
	 * Devuelve la velocidad de un pokemon
	 * @return int con la defensa de un pokemon
	 */
	public int getVelocidad() {
		return velocidad;
	}

	/**
	 * Establece la velocidad de un pokemon
	 * @param velocidad int con la velocidad de un pokemon
	 */
	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}

	/**
	 * Devuelve la id del pokemon
	 * @return int con el id del pokemon
	 */
	public int getId() {
		return id;
	}

	/**
	 * Devuelve el nombre del pokemon
	 * @return devuelve la variable nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Devuelve el nivel del pokemon
	 * @return devuelve la variable nivel
	 */
	public int getNivel() {
		return nivel;
	}

	/**
	 * Devuelve la lista de tipos del pokemon
	 * @return devuelve el array tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Devuelve la lista de ataques del pokemon
	 * @return devuelve el array ataques
	 */
	public Ataque[] getAtaques() {
		return ataques;
	}

	/**
	 * Devuelve la vida del pokemon
	 * @return devuelve la variable ps
	 */
	public int getPs() {
		return ps;
	}

	/**
	 * Modifica la variable ps
	 * @param ps
	 */
	public void setPs(int ps) {
		this.ps = ps;
	}

	/**
	 * Devuelve la vida original del pokemon
	 * @return devuelve la variable psOriginal
	 */
	public int getPsOriginal() {
		return psOriginal;
	}

	/**
	 * Devuelve el ataque del pokemon
	 * @return devuelve la variable ataque
	 */
	public int getAtaque() {
		return ataque;
	}

	/**
	 * Devuelve la defensa del pokemon
	 * @return devuelve la variable defensa
	 */
	public int getDefensa() {
		return defensa;
	}

	/**
	 * Devuelve el momento de captura del pokemon
	 * @return devuelve la variable momentoCaptura
	 */
	public Timestamp getMomentoCaptura() {
		return momentoCaptura;
	}

}
