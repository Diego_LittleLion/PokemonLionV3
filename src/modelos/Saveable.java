package modelos;

import java.sql.Connection;

import excepciones.CargarPartidaException;

/**
 * Interface de grabado
 * @author DIEGO LITTLELION
 *
 */
public interface Saveable {
   boolean writeToDB(Connection con) throws CargarPartidaException;
}
