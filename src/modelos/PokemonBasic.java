package modelos;

/**
 * Clase encargada de almacenar los datos leidos de un JSON
 * @author DIEGO LITTLELION
 *
 */
public class PokemonBasic {
	private int ps;
	private int ataque;
	private int defensa;
	private int ataqueEspecial;
	private int defensaEspecial;
	private int velocidad;
	
	/**
	 * Constructor de la clase PokemonBasic
	 * @param ps int con la vida del pokemon
	 * @param ataque int con el ataque del pokemon
	 * @param defensa int con la defensa del pokemon
	 * @param ataqueEspecial int con el ataque especial del pokemon
	 * @param defensaEspecial int con la defensa especial del pokemon
	 * @param velocidad int con la velocidad del pokemon
	 */
	public PokemonBasic(int ps,int ataque,int defensa,int ataqueEspecial,int defensaEspecial,int velocidad) {
		this.ps=ps;
		this.ataque=ataque;
		this.defensa=defensa;
		this.ataqueEspecial=ataqueEspecial;
		this.defensaEspecial=defensaEspecial;
		this.velocidad=velocidad;
	}

	/**
	 * Devuelve la vida del pokemon
	 * @return int con la vida del pokemon
	 */
	public int getPs() {
		return ps;
	}

	/**
	 * Establece la vida del pokemon
	 * @param ps int con la vida del pokemon
	 */
	public void setPs(int ps) {
		this.ps = ps;
	}

	/**
	 * Devuelve el ataque del pokemon
	 * @return int con el ataque del pokemon
	 */
	public int getAtaque() {
		return ataque;
	}

	/**
	 * Establece el ataque del pokemon
	 * @param ataque int con el ataque del pokemon
	 */
	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}

	/**
	 * Devuelve la defensa del pokemon
	 * @return int con la defensa del pokemon
	 */
	public int getDefensa() {
		return defensa;
	}

	/**
	 * Establece la defensa del pokemon
	 * @param defensa int con la defensa del pokemon
	 */
	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}

	/**
	 * Devuelve el ataque especial del pokemon
	 * @return int con el ataque especial del pokemon
	 */
	public int getAtaqueEspecial() {
		return ataqueEspecial;
	}

	/**
	 * Establece el ataque especial del pokemon
	 * @param ataqueEspecial int con el ataque especial del pokemon
	 */
	public void setAtaqueEspecial(int ataqueEspecial) {
		this.ataqueEspecial = ataqueEspecial;
	}

	/**
	 * Devuelve la defensa especial del pokemon
	 * @return int con la defensa especial del pokemon
	 */
	public int getDefensaEspecial() {
		return defensaEspecial;
	}

	/**
	 * Establece la defensa especial del pokemon
	 * @param defensaEspecial int con la defensa especial del pokemon
	 */
	public void setDefensaEspecial(int defensaEspecial) {
		this.defensaEspecial = defensaEspecial;
	}

	/**
	 * Devuelve la velocidad del pokemon
	 * @return int con la velocidad del pokemon
	 */
	public int getVelocidad() {
		return velocidad;
	}

	/**
	 * Establece la velocidad del pokemon
	 * @param velocidad int con la velocidad del pokemon
	 */
	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
}
