package modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import excepciones.CargarPartidaException;
import objetos.Pocion;
import objetos.Pokeball;

/**
 * Clase Partida, encargada de gestionar los datos de la partida
 * @author DIEGO LITTLELION
 *
 */
public class Partida implements Saveable {
	private int id;
	private String nombreJugador;
	private String generoJugador;
	private String fechaNacimientoJugador;
	private int pokecuartos;
	private ArrayList<Pokemon> pokemones = new ArrayList<Pokemon>();
	private ArrayList<Objeto> objetos = new ArrayList<Objeto>();

	/**
	 * Constructor de la partida
	 * @param id int con el número de la partida
	 * @param nombreJugador String con el nombre del jugador
	 * @param generoJugador String con el genero del jugador
	 * @param fechaNacimientoJugador String con la fecha de nacimiento
	 * @param pokecuartos int con los pokecuartos del jugador
	 */
	public Partida(int id, String nombreJugador, String fechaNacimientoJugador, String generoJugador, int pokecuartos) {
		this.id = id;
		this.nombreJugador = nombreJugador;
		this.generoJugador = generoJugador;
		this.fechaNacimientoJugador = fechaNacimientoJugador;
		this.pokecuartos = pokecuartos;
	}

	// Métodos de los pokemon
	/**
	 * Añade un nuevo pokemon al jugador
	 * @param poke Pokemon para añadir
	 */
	public void aniadirPokemon(Pokemon poke) {
		pokemones.add(poke);
	}

	/**
	 * Devuelve un Pokemon
	 * @param id ID del pokemon a devolver
	 * @return Pokemon devuelto
	 */
	public Pokemon getPokemon(int id) {
		return pokemones.get(id);
	}

	/**
	 * Libera un Pokemon
	 * @param id ID del pokemon a liberar
	 */
	public void liberarPokemon(int id) {
		pokemones.remove(id);
	}

	/**
	 * Devuelve la lista de todos los pokemon
	 * @return ArrayList con todos los pokemon
	 */
	public ArrayList<Pokemon> getListadoPokemon() {
		return pokemones;
	}

	// Métodos objetos
	/**
	 * Devuelve la lista de objetos
	 * @return ArrayList con todos los objetos
	 */
	public ArrayList<Objeto> getListadoObjetos() {
		return objetos;
	}

	/**
	 * Añade pokeballs a la partida
	 * @param cantidadAniadir int con la cantidad de pokeball para añadir
	 * @param probabilidad int con la probabilidad de la pokeball
	 */
	public void aniadirPokeball(int cantidadAniadir, int probabilidad) {
		Pokeball pokeball = new Pokeball("pokeball", probabilidad);
		for (int i = 0; i < cantidadAniadir; i++) {
			objetos.add(pokeball);
		}
	}

	/**
	 * Elimina una pokeball
	 */
	public void eliminarPokeball() {
		int cantidad = 0;
		for (int i = 0; i < objetos.size(); i++) {
			if (objetos.get(i).getNombre().equals("pokeball")) {
				cantidad++;
			}
		}
		objetos.remove(cantidad - 1);
	}

	/**
	 * Devuelve la cantidad de pokeballs del jugador
	 * @return int Con la cantidad de pokeballs
	 */
	public int getCantidadPokeballs() {
		int cantidad = 0;
		for (int i = 0; i < objetos.size(); i++) {
			if (objetos.get(i).getNombre().equals("pokeball")) {
				cantidad++;
			}
		}
		return cantidad;
	}

	/**
	 * Añade pociones a la partida
	 * @param cantidadAniadir int con la cantidad de pociones para añadir
	 * @param cantidadCuracion int con la curación de la poción
	 */
	public void aniadirPocion(int cantidadAniadir, int cantidadCuracion) {
		Pocion pocion = new Pocion("pocion", 20);
		for (int i = 0; i < cantidadAniadir; i++) {
			objetos.add(pocion);
		}
	}

	/**
	 * Devuelve la cantidad de pociones del jugador
	 * @return int con la cantidad de pociones del jugador
	 */
	public int getCantidadPociones() {
		int cantidad = 0;
		for (int i = 0; i < objetos.size(); i++) {
			if (objetos.get(i).getNombre().equals("pocion")) {
				cantidad++;
			}
		}
		return cantidad;
	}

	/**
	 * Elimina una poción
	 */
	public void eliminarPocion() {
		int cantidad = 0;
		for (int i = 0; i < objetos.size(); i++) {
			if (objetos.get(i).getNombre().equals("pocion")) {
				cantidad++;
			}
		}
		objetos.remove(cantidad - 1);
	}

	// Métodos jugador
	/**
	 * Devuelve la cantidad de pokecuartos
	 * @return int con la cantidad de pokecuartos
	 */
	public int getPokecuartos() {
		return pokecuartos;
	}

	/**
	 * Devuelve la id de la partida
	 * @return int con la id de la partida
	 */
	public int getId() {
		return id;
	}

	/**
	 * Establece la cantidad de pokecuartos
	 * @param pokecuartos int con la cantidad de pokecuartos
	 */
	public void setPokecuartos(int pokecuartos) {
		this.pokecuartos = pokecuartos;
	}

	/**
	 * Devuelve el nombre del jugador
	 * @return String con el nombre del jugador
	 */
	public String getNombreJugador() {
		return nombreJugador;
	}

	/**
	 * Establece el género del jugador
	 * @return String con el género del jugador
	 */
	public String getGeneroJugador() {
		return generoJugador;
	}

	/**
	 * Devuelve la fecha de nacimiento del jugador
	 * @return String con la fecha de nacimiento
	 */
	public String getFechaNacimientoJugador() {
		return fechaNacimientoJugador;
	}

	@Override
	/**
	 * Escribe en la base de datos el estado de la partida
	 */
	public boolean writeToDB(Connection con) throws CargarPartidaException {
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("DELETE FROM Partidas WHERE id=" + this.getId());
			stmt.executeUpdate();
			stmt = con.prepareStatement("DELETE FROM Objetos WHERE idPartida=" + this.getId());
			stmt.executeUpdate();
			stmt = con.prepareStatement("DELETE FROM Pokemons WHERE idPartida=" + this.getId());
			stmt.executeUpdate();
			stmt = con.prepareStatement("INSERT INTO Partidas VALUES (?,?,?,?,?)");
			stmt.setInt(1, this.getId());
			stmt.setString(2, this.getNombreJugador());
			stmt.setString(3, this.getFechaNacimientoJugador());
			stmt.setString(4, this.getGeneroJugador());
			stmt.setInt(5, this.getPokecuartos());
			stmt.executeUpdate();
			int cantidadPokeball = 0;
			int cantidadPociones = 0;

			stmt = con.prepareStatement("INSERT INTO Objetos VALUES (?,?,?,?)");
			for (int i = 0; i < this.objetos.size(); ++i) {
				if (objetos.get(i).getNombre().equals("pokeball")) {
					cantidadPokeball++;
				} else {
					cantidadPociones++;
				}
			}
			stmt.setInt(1, this.getId());
			stmt.setInt(2, 1);
			stmt.setString(3, "pokeball");
			stmt.setInt(4, cantidadPokeball);
			stmt.executeUpdate();
			stmt.setInt(1, this.getId());
			stmt.setInt(2, 2);
			stmt.setString(3, "pocion");
			stmt.setInt(4, cantidadPociones);
			stmt.executeUpdate();
			for (int i = 0; i < this.getListadoPokemon().size(); ++i) {
				stmt = con.prepareStatement("INSERT INTO Pokemons VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
				stmt.setInt(1, this.getId());
				stmt.setInt(2, i);
				stmt.setString(3, this.getPokemon(i).getClass().getSimpleName());
				stmt.setString(4, this.getPokemon(i).getNombre());
				stmt.setInt(5, this.getPokemon(i).getNivel());
				stmt.setString(6, this.getPokemon(i).getTipo());
				stmt.setInt(7, this.getPokemon(i).getPs());
				stmt.setInt(8, this.getPokemon(i).getAtaque());
				stmt.setInt(9, this.getPokemon(i).getDefensa());
				stmt.setInt(10, this.getPokemon(i).getAtaqueEspecial());
				stmt.setInt(11, this.getPokemon(i).getDefensaEspecial());
				stmt.setInt(12, this.getPokemon(i).getVelocidad());
				stmt.executeUpdate();
			}
			con.close();
			
		} catch (SQLException ex) {
			throw new CargarPartidaException();
		}
		return false;
	}
}
