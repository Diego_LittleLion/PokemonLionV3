package pokemonLionV3;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;


import ataques.Ataque;
import conexion.Conexion;
import conexion.DbDatos;
import conexion.Json;
import excepciones.CargarPartidaException;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import modelos.Partida;
import modelos.Pokemon;
import pokemon.*;

/**
 * Clase controladora de la interfaz
 * 
 * @author DIEGO LITTLELION
 */
public class Controlador implements Initializable {
	// Formula aplicada para las stats de los Pokemon
	// https://pokemondb.net/pokebase/188241/how-do-you-calculate-the-base-stats-for-each-pokemon
	// Formula aplicada para el daño
	// https://pokemon.fandom.com/es/wiki/Da%C3%B1o
	// Alertas creadas con esta guía https://code.makery.ch/blog/javafx-dialogs-official/
	// Poner a 0 para que muestre todo del dialogo de Oak y en la función nuevaPartidaBotonAccion()
	Random r = new Random();
	int contadorDialogo = 0;
	Partida partida;
	int x = 96;
	int y = 167;
	int pasosDados = 0;
	int pasosRandom;
	
	// Variables de pokemon
	static int numPokemon = 1; // ID
	String especie;
	int nivel;
	Caterpie cat;
	Lickitung lick;
	Pidgey pid;
	Pikachu pika;
	Rayquaza ray;
	Squirtle squirt;
	Enemigo enemigo;
	
	// AnchorPane
	@FXML private AnchorPane root;
	
	// Paneles
	@FXML private Pane anteriorPanel;
	@FXML private Pane actualPanel;
	@FXML private Pane inicioPanel;
	@FXML private Pane nuevaPartidaPanel;
	@FXML private Pane formularioJugador;
	@FXML private Pane principalPanel;
	@FXML private Pane opcionesPanel;
	@FXML private Pane cargarPanel;
	@FXML private Pane informacionJugadorPanel;
	@FXML private Pane tiendaPanel;
	@FXML private Pane pokemonesPanel;
	@FXML private Pane explorarPanel;
	@FXML private Pane capturaPokemon;
	@FXML private Pane eleccionPokemonCombate;
	@FXML private Pane combatePanel;
	@FXML private Pane pokecenterPanel;

	// Botones
	@FXML private Button nuevaPartidaBoton;
	@FXML private Button avanzarChatOak;
	@FXML private Button crearJugadorBoton;
	@FXML private Button cargarPartidaBoton;
	@FXML private Button capturarBoton;
	@FXML private Button guardarPokemonCapturadoBoton;
	@FXML private Button ataque1Boton;
	@FXML private Button ataque2Boton;
	@FXML private Button ataque3Boton;
	@FXML private Button ataque4Boton;
	@FXML private Button curarCombateBoton;

	// Labels
	@FXML private Label chatOakLB;
	@FXML private Label pokeballsMenuLB;
	@FXML private Label pokecuartosMenuLB;
	@FXML private Label idPartidaLB;
	@FXML private Label nombreJugadorLB;
	@FXML private Label generoJugadorLB;
	@FXML private Label fechaNacimientoJugadorLB;
	@FXML private Label pokecuartosJugadorLB;
	@FXML private Label pokeballsJugadorLB;
	@FXML private Label pocionesJugadorLB;
	@FXML private Label pokemonesJugadorLB;
	@FXML private Label cantidadPokecuartosTiendaLB;
	@FXML private Label avisoCompraLB;
	@FXML private Label infoPokemonNombreLB;
	@FXML private Label infoPokemonNivelLB;
	@FXML private Label infoPokemonTipoLB;
	@FXML private Label infoPokemonAtaqueLB;
	@FXML private Label infoPokemonDefensaLB;
	@FXML private Label infoPokemonAtaquesLB;
	@FXML private Label infoPokemonFechaLB;
	@FXML private Label tituloPokemonCapturaLB;
	@FXML private Label nivelPokemonCapturaLB;
	@FXML private Label mensajePokemonCapturaLB;
	@FXML private Label vidaAliadoCombateLB;
	@FXML private Label vidaEnemigoCombateLB;
	@FXML private Label nombreAliadoInfoLB;
	@FXML private Label nivelAliadoInfoLB;
	@FXML private Label tipoAliadoInfoLB;
	@FXML private Label ataqueAliadoInfoLB;
	@FXML private Label defensaAliadoInfoLB;
	@FXML private Label ataquesAliadoInfoLB;
	@FXML private Label capturaAliadoInfoLB;
	@FXML private Label mensajeCuracionLB;
	@FXML private Label mensajeCombateLB;
	
	// TextFields
	@FXML private TextField nombreJugadorTF;
	@FXML private TextField motePokemonCapturaFT;
	@FXML private DatePicker fechaNacimientoDate;

	// ChoiceBox/ComboBox
	@FXML private ComboBox<String> generoCB;
	@FXML private ComboBox<String> selectorPartidaCB;
	@FXML private ComboBox<Integer> selectorPokeballsCB;
	@FXML private ComboBox<Integer> selectorPocionesCB;
	@FXML private ComboBox<String> infoPokemonCB;
	@FXML private ComboBox<String> selectorPokemonCombateCB;

	// ImageView
	@FXML private ImageView imagenJugador;
	@FXML private ImageView imagenPersonaje;
	@FXML private ImageView imagenPokemonInfo;
	@FXML private ImageView imagenPokemonCaptura;
	@FXML private ImageView imagenPokemonCombatir;
	@FXML private ImageView imagenAliadoInfo;
	@FXML private ImageView imgAliadoCombate;
	@FXML private ImageView imgEnemigoCombate;

	/**
	 * Función que oculta el panel de incio y muestra el panel de creación de nueva
	 * partida
	 * 
	 * @throws IOException
	 */
	@FXML public void nuevaPartidaBotonAccion() {
		contadorDialogo = 0;
		anteriorPanel = inicioPanel;
		actualPanel = nuevaPartidaPanel;
		inicioPanel.setVisible(false);
		nuevaPartidaPanel.setVisible(true);
		chatOakLB.setVisible(true);
		avanzarChatOak.setVisible(true);
		formularioJugador.setVisible(false);
		chatOakLB.setText("Hola, encantado de conocerte!");
	}

	/**
	 * Función para avanzar en el chat de oak, se ejecutará cada vez que le demos al
	 * botón "siguiente"
	 */
	@FXML public void avanzarChatOak() {
		String[] dialogoOak = { "¡Estas en el mundo de los pokemon!", "Me llamo Oak, pero me llaman profesor Pokemon",
				"Puedo hablarte de todo lo que necesites saber de este mundo",
				"Este mundo esta habitado por criatura llamadas Pokemom",
				"Los humanos y los Pokemon trabajamos juntos como amigos",
				"Hay quien combate con sus Pokemon para estrechar lazos con ellos",
				"Yo me dedico a estudiar los Pokemon para conocerlos mejor",
				"Bueno, que te parece si ahora me cuentas algo sobre ti?",
				"Tu propia aventura está apunto de comenzar!", "Viviras aventuras emocionantes",
				"Y pasaras momentos dificiles", "Ahora...", "Entra en el mundo Pokemon!" };
		if (contadorDialogo == 8) {
			chatOakLB.setVisible(false);
			avanzarChatOak.setVisible(false);
			formularioJugador.setVisible(true);
			// Ingresamos las opciones de género en el initialize()
		} else if (contadorDialogo == 13) {
			nuevaPartidaPanel.setVisible(false);
			principalPanel.setVisible(true);
			pokeballsMenuLB.setText(Integer.toString(partida.getCantidadPokeballs()));
			pokecuartosMenuLB.setText(Integer.toString(partida.getPokecuartos()));
		} else {
			chatOakLB.setText(dialogoOak[contadorDialogo]);
		}
		contadorDialogo++;
	}

	/**
	 * Función para mostrar el formulario de datos del jugador
	 */
	@FXML public void getDatosNuevoJugador() {
		String nombreJugador = nombreJugadorTF.getText();
		String fechaNacimientoJugador = fechaNacimientoDate.getEditor().getText();
		String genero = generoCB.getValue();
		
		if(!nombreJugador.equals("") && !fechaNacimientoJugador.equals("") && genero != null) {
			crearNuevoJugador(nombreJugador, fechaNacimientoJugador, genero);	
		} else {
			mostrarAlerta("¡Debes rellenar el formulario!");
		}
	}

	/**
	 * Función para crear el nuevo jugador cogiendo la id que le correspondería en
	 * la BBDD
	 * 
	 * @param genero                 Genero del jugador
	 * @param fechaNacimientoJugador Fecha de nacimiento del jugador
	 * @param nombreJugador          Nombre del jugador
	 */
	public void crearNuevoJugador(String nombreJugador, String fechaNacimientoJugador, String genero) {
		try {
			Conexion cx = new Conexion("SELECT max(id) as id from Partidas limit 1");
			int id = 0;
			while (cx.getResult().next()) {
				id = cx.getResult().getInt("id");
			}
			chatOakLB.setVisible(true);
			avanzarChatOak.setVisible(true);
			formularioJugador.setVisible(false);

			// Creamos la partida
			int pokecuartos = 500;
			partida = new Partida((id + 1), nombreJugador, genero, fechaNacimientoJugador, pokecuartos);
			partida.aniadirPokeball(10, 20);
			partida.aniadirPocion(5, 20);
			chatOakLB.setText("Ha llegado el momento...");
			cx.close();
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	/**
	 * Función para mostrar las partidas disponibles para cargar
	 * 
	 */
	@FXML public void seleccionCargarPartida() {
		inicioPanel.setVisible(false);
		cargarPanel.setVisible(true);
		anteriorPanel = inicioPanel;
		actualPanel = cargarPanel;
		selectorPartidaCB.getItems().clear();
		try {
			Conexion cx = new Conexion("SELECT id, nombre, pokecuartos FROM Partidas");
			while (cx.getResult().next()) {
				selectorPartidaCB.getItems()
						.add(cx.getResult().getString("id") + "  | Jugador: " + cx.getResult().getString("nombre")
								+ " | Pokecuartos: " + cx.getResult().getString("pokecuartos"));
			}
			cx.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Función para cargar los datos de la partida seleccionada
	 */
	@FXML public void cargarPartidaSeleccionada() {
		int idSeleccionado = selectorPartidaCB.getSelectionModel().getSelectedIndex() + 1;
		try {
			Conexion cx = new Conexion("SELECT * FROM Partidas WHERE id = " + idSeleccionado);
			while (cx.getResult().next()) {
				partida = new Partida(cx.getResult().getInt("id"), cx.getResult().getString("nombre"),
						cx.getResult().getString("fechaNacimiento"), cx.getResult().getString("genero"), cx.getResult().getInt("pokecuartos"));
			}
			cx.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		cargarObjetos();
		cargarPokemons();
		cargarPanel.setVisible(false);
		principalPanel.setVisible(true);
		pokeballsMenuLB.setText(Integer.toString(partida.getCantidadPokeballs()));
		pokecuartosMenuLB.setText(Integer.toString(partida.getPokecuartos()));
	}
	
	/**
	 * Función para cargar los objetos de una partida
	 */
	public void cargarObjetos() {
		try {
			Conexion cx = new Conexion("SELECT * FROM Objetos WHERE idPartida = " + partida.getId());
			int cantidadPokeballs = 0;
			int cantidadPociones = 0;
			while (cx.getResult().next()) {
				if (cx.getResult().getInt("idObjeto") == 1) {
					cantidadPokeballs = cx.getResult().getInt("cantidad");
				}
				if (cx.getResult().getInt("idObjeto") == 2) {
					cantidadPociones = cx.getResult().getInt("cantidad");
				}
			}
			partida.aniadirPokeball(cantidadPokeballs, 20);
			partida.aniadirPocion(cantidadPociones, 20);
			cx.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Función para cargar los pokemon de una partida
	 */
	public void cargarPokemons() {
		try {
			Conexion cx = new Conexion("SELECT * FROM Pokemons WHERE idPartida = " + partida.getId());
			String especie;
			ResultSet resultado = cx.getResult();
			while (resultado.next()) {
				especie = resultado.getString("especie");
				if (especie.equals("Caterpie")) {
					cat = new Caterpie(resultado.getInt("idPokemon"), resultado.getString("nombre"),
							resultado.getInt("nivel"));
					cat.setPs(resultado.getInt("ps"));
					partida.aniadirPokemon(cat);
				} else if (especie.equals("Lickitung")) {
					lick = new Lickitung(resultado.getInt("idPokemon"), resultado.getString("nombre"),
							resultado.getInt("nivel"));
					lick.setPs(resultado.getInt("ps"));
					partida.aniadirPokemon(lick);
				} else if (especie.equals("Pidgey")) {
					pid = new Pidgey(resultado.getInt("idPokemon"), resultado.getString("nombre"),
							resultado.getInt("nivel"));
					pid.setPs(resultado.getInt("ps"));
					partida.aniadirPokemon(pid);
				} else if (especie.equals("Pikachu")) {
					pika = new Pikachu(resultado.getInt("idPokemon"), resultado.getString("nombre"),
							resultado.getInt("nivel"));
					pika.setPs(resultado.getInt("ps"));
					partida.aniadirPokemon(pika);
				} else if (especie.equals("Rayquaza")) {
					ray = new Rayquaza(resultado.getInt("idPokemon"), resultado.getString("nombre"),
							resultado.getInt("nivel"));
					ray.setPs(resultado.getInt("ps"));
					partida.aniadirPokemon(ray);
				} else if (especie.equals("Squirtle")) {
					squirt = new Squirtle(resultado.getInt("idPokemon"), resultado.getString("nombre"),
							resultado.getInt("nivel"));
					squirt.setPs(resultado.getInt("ps"));
					partida.aniadirPokemon(squirt);
				}
			}
			cx.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Función para mostrar los datos del jugador cuando se pulsa el botón
	 */
	@FXML public void mostrarInformacion() {
		anteriorPanel = principalPanel;
		actualPanel = informacionJugadorPanel;
		principalPanel.setVisible(false);
		informacionJugadorPanel.setVisible(true);
		// Mostramos la imagen del jugador
		try {
			if (partida.getGeneroJugador().equals("Chico")) {
				File imagenFile = new File("assets/imagenes/chico.png");
				Image img = new Image(imagenFile.toURI().toString());
				imagenJugador.setImage(img);
			} else if (partida.getGeneroJugador().equals("Chica")) {
				File imagenFile = new File("assets/imagenes/chica.png");
				Image img = new Image(imagenFile.toURI().toString());
				imagenJugador.setImage(img);
			} else {
				File imagenFile = new File("assets/imagenes/interrogacion.png");
				Image img = new Image(imagenFile.toURI().toString());
				imagenJugador.setImage(img);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Mostramos los datos del jugador
		idPartidaLB.setText("ID de partida: " + partida.getId());
		nombreJugadorLB.setText("Nombre del jugador: " + partida.getNombreJugador());
		generoJugadorLB.setText("Género: " + partida.getGeneroJugador());
		fechaNacimientoJugadorLB.setText("Fecha de nacimiento: " + partida.getFechaNacimientoJugador());
		pokecuartosJugadorLB.setText("Pokecuartos: " + partida.getPokecuartos());
		pokeballsJugadorLB.setText("Pokeballs: " + partida.getCantidadPokeballs());
		pocionesJugadorLB.setText("Pociones: " + partida.getCantidadPociones());
		pokemonesJugadorLB.setText("Pokemons: " + partida.getListadoPokemon().size());
	}

	/**
	 * Función para mostrar la tienda con sus objetos
	 */
	@FXML public void irTienda() {
		anteriorPanel = principalPanel;
		actualPanel = tiendaPanel;
		principalPanel.setVisible(false);
		tiendaPanel.setVisible(true);
		selectorPokeballsCB.getItems().clear();
		selectorPocionesCB.getItems().clear();
		selectorPokeballsCB.getItems().addAll(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		selectorPokeballsCB.getSelectionModel().selectFirst();
		selectorPocionesCB.getItems().addAll(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		selectorPocionesCB.getSelectionModel().selectFirst();
		cantidadPokecuartosTiendaLB.setText("Tienes: " + partida.getPokecuartos() + " Pokecuartos");
	}

	/**
	 * Función para comprar los objetos elegidos
	 */
	@FXML public void comprar() {
		int cantidadPokeballs = selectorPokeballsCB.getValue();
		int cantidadPociones = selectorPocionesCB.getValue();
		int precioPokeballs = cantidadPokeballs * 100;
		int precioPociones = cantidadPociones * 200;
		int precioFinal = precioPokeballs + precioPociones;
		avisoCompraLB.setVisible(true);
		if (precioFinal == 0) {
			avisoCompraLB.setText("Tienes el carrito vacio");
		} else if (partida.getPokecuartos() >= precioFinal) {
			partida.setPokecuartos(partida.getPokecuartos() - precioFinal);
			partida.aniadirPokeball(cantidadPokeballs, 20);
			partida.aniadirPocion(cantidadPociones, 20);
			avisoCompraLB.setText("Compra realizada satisfactoriamente");
			cantidadPokecuartosTiendaLB.setText("Tienes: " + partida.getPokecuartos() + " Pokecuartos");
		} else {
			avisoCompraLB.setText("No tienes suficientes pokecuartos");
		}
	}
	
	/**
	 * Función para ir al Pokecenter
	 */
	@FXML public void irPokecenter() {
		anteriorPanel = principalPanel;
		actualPanel = pokecenterPanel;
		principalPanel.setVisible(false);
		pokecenterPanel.setVisible(true);
		mensajeCuracionLB.setVisible(false);
	}
	
	/**
	 * Función para curar a los pokemon
	 */
	@FXML public void curar() {
		ArrayList<Pokemon> pokemons = partida.getListadoPokemon();
		for(int i = 0; i < pokemons.size(); i++) {
			pokemons.get(i).setPs(pokemons.get(i).getPsOriginal());
			for(int j = 0; j < pokemons.get(i).getAtaques().length; j++) {
				pokemons.get(i).getAtaques()[j].setPp(pokemons.get(i).getAtaques()[j].getPpOriginal());
			}
		}
		mensajeCuracionLB.setVisible(true);
	}

	/**
	 * Función para mostrar el listado de los pokemon para mostrar su información
	 */
	@FXML public void listarPokemons() {
		anteriorPanel = principalPanel;
		actualPanel = pokemonesPanel;
		principalPanel.setVisible(false);
		pokemonesPanel.setVisible(true);
		infoPokemonCB.getItems().clear();
		resetInfoPokemon();
		for (int i = 0; i < partida.getListadoPokemon().size(); i++) {
			infoPokemonCB.getItems().add("Especie: " + partida.getPokemon(i).getClass().getSimpleName() + " | Mote: "
					+ partida.getPokemon(i).getNombre() + " | Nivel: " + partida.getPokemon(i).getNivel());
		}
	}

	/**
	 * Función para resetear los valores del panel de información pokemon
	 */
	public void resetInfoPokemon() {
		infoPokemonNombreLB.setText("Nombre: ");
		infoPokemonNivelLB.setText("Nivel: ");
		infoPokemonTipoLB.setText("Tipo: ");
		infoPokemonAtaqueLB.setText("Ataque: ");
		infoPokemonDefensaLB.setText("PS: | Defensa: ");
		infoPokemonAtaquesLB.setText("Ataques: ");
		infoPokemonFechaLB.setText("Fecha de captura: ");
		File imagenPoke = new File("assets/imagenes/pokeball.png");
		Image img = new Image(imagenPoke.toURI().toString());
		imagenPokemonInfo.setImage(img);
	}	
	
	/**
	 * Función para ver la información del pokemon seleccionado
	 */
	@FXML public void mostrarInfoPokemon() {
		int pokemonElegido = infoPokemonCB.getSelectionModel().getSelectedIndex();
		if (pokemonElegido != -1) {
			Pokemon pokeElegido = partida.getPokemon(pokemonElegido);
			String ataques = "";
			for (int i = 0; i < pokeElegido.getAtaques().length; i++) {
				ataques = ataques + pokeElegido.getAtaques()[i].getNombre() + " | ";
			}
			ataques = ataques.trim();
			infoPokemonNombreLB.setText("Nombre: " + pokeElegido.getNombre());
			infoPokemonNivelLB.setText("Nivel: " + pokeElegido.getNivel());
			infoPokemonTipoLB.setText("Tipo: " + pokeElegido.getTipo());
			infoPokemonAtaqueLB.setText(
					"Ataque: " + pokeElegido.getAtaque() + " | Ataque Especial: " + pokeElegido.getAtaqueEspecial());
			infoPokemonDefensaLB.setText("PS: " + pokeElegido.getPs() + "/" + pokeElegido.getPsOriginal() + " | Defensa: " + pokeElegido.getDefensa()
					+ " | Defensa Especial: " + pokeElegido.getDefensaEspecial());
			infoPokemonAtaquesLB.setText("Ataques: " + ataques);
			infoPokemonFechaLB.setText("Fecha de captura: " + pokeElegido.getMomentoCaptura());
			File imagenPoke = new File("assets/imagenes/pokemons/" + pokeElegido.getClass().getSimpleName() + ".gif");
			Image img = new Image(imagenPoke.toURI().toString());
			imagenPokemonInfo.setImage(img);
		}
	}
	
	/**
	 * Función para liberar un pokemon
	 */
	@FXML public void liberarPokemon() {
		int pokemonElegido = infoPokemonCB.getSelectionModel().getSelectedIndex();
		String mensaje;
		if(pokemonElegido != -1) {
			mensaje = "Pokemon liberado correctamente";
			mostrarAlerta(mensaje);
			partida.liberarPokemon(pokemonElegido);
			resetInfoPokemon();
			infoPokemonCB.getItems().remove(pokemonElegido);
		} else {
			mensaje = "No has elegido ningún pokemon";
			mostrarAlerta(mensaje);
		}	
	}
	
	/**
	 * Función para mostrar el panel de exploración	y poder movernos
	 */
	@FXML public void explorar() {
		anteriorPanel = principalPanel;
		actualPanel = explorarPanel;
		principalPanel.setVisible(false);
		explorarPanel.setVisible(true);
		// Generamos un número random de pasos random a dar
		pasosRandom = (int) (Math.random() * (75 - 5) + 5);
		pasosDados = 0;
		x = 96;
		y = 167;
		imagenPersonaje.setLayoutX(x);
		imagenPersonaje.setLayoutY(y);
		explorarPanel.setOnKeyPressed(new EventHandler<KeyEvent>() {
	        @Override
	        public void handle(KeyEvent event) {
	        	 if(event.getCode() == KeyCode.UP) {
	        		File imagenFile = new File("assets/imagenes/personaje/espalda.png");
	 				Image img = new Image(imagenFile.toURI().toString());
	 				imagenPersonaje.setImage(img);
	 				y -= 8;
	 				if(y < 0) {
	 					y = 0;
	 					imagenPersonaje.setLayoutY(y);
	 				} else {
	 					imagenPersonaje.setLayoutY(y);
	 				}
	 				pasosDados++;
	 				comprobarPasosDados(pasosRandom);
	             }
	        	 if(event.getCode() == KeyCode.DOWN) {
	        		File imagenFile = new File("assets/imagenes/personaje/cara.png");
		 			Image img = new Image(imagenFile.toURI().toString());
		 			imagenPersonaje.setImage(img);
		 			y += 8;
		 			if(y > 333) {
	 					y = 333;
	 					imagenPersonaje.setLayoutY(y);
	 				} else {
	 					imagenPersonaje.setLayoutY(y);
	 				}
		 			pasosDados++;
		 			comprobarPasosDados(pasosRandom);
	             }
	        	 if(event.getCode() == KeyCode.LEFT) {
	        		File imagenFile = new File("assets/imagenes/personaje/mirandoIzquierda.png");
		 			Image img = new Image(imagenFile.toURI().toString());
		 			imagenPersonaje.setImage(img);
		 			x -= 8;
		 			if(x < 0) {
	 					x = 0;
	 					imagenPersonaje.setLayoutX(x);
	 				} else {
	 					imagenPersonaje.setLayoutX(x);
	 				}
		 			pasosDados++;
		 			comprobarPasosDados(pasosRandom);
	             }
	        	 if(event.getCode() == KeyCode.RIGHT) {
	        		File imagenFile = new File("assets/imagenes/personaje/mirandoDerecha.png");
		 			Image img = new Image(imagenFile.toURI().toString());
		 			imagenPersonaje.setImage(img);
	        		x += 8;
	        		if(x > 1116) {
	 					x = 1116;
	 					imagenPersonaje.setLayoutX(x);
	 				} else {
	 					imagenPersonaje.setLayoutX(x);
	 				}
	        		pasosDados++;
	        		comprobarPasosDados(pasosRandom);
	             }
	        }
	    });
	}
	
	/**
	 * Función para comprobar si ya hemos dado los pasos necesarios para capturar/combatir un pokemon
	 * @param pasosRandom Pasos generados de forma random
	 */
	public void comprobarPasosDados(int pasosRandom) {
		String especieEncontrada = "";
		if(pasosDados >= pasosRandom) {
			int especieRandom = (int) (Math.random() * 6) + 1;
			nivel = (int) (Math.random() * 100) + 1;
			if (especieRandom == 1) {
				especieEncontrada = "Caterpie";
			} else if(especieRandom == 2) {
				especieEncontrada = "Lickitung";
			} else if(especieRandom == 3) {
				especieEncontrada = "Pidgey";
			} else if(especieRandom == 4) {
				especieEncontrada = "Pikachu";
			} else if(especieRandom == 5) {
				especieEncontrada = "Rayquaza";
			} else if(especieRandom == 6) {
				especieEncontrada = "Squirtle";
			}
			especie = especieEncontrada;
			popupPokemonEncontrado(especieEncontrada);
		}
	}
	
	/**
	 * Función para mostrar un popUp cuando se encuentre un pokemon
	 * @param especieEncontrada Especie generada de forma random
	 */
	public void popupPokemonEncontrado(String especieEncontrada) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
		File imagenFile = new File("assets/imagenes/miniPokeball.png");
		stage.getIcons().add(new Image(imagenFile.toURI().toString()));
		alert.getDialogPane().setGraphic(new ImageView(imagenFile.toURI().toString()));
		alert.setTitle("Pokemon encontrado!");
		alert.setHeaderText(null);
		alert.setContentText("Ha aparecido un " + especieEncontrada + " de nivel " + nivel + ", ¿Qué deseas hacer?");
		ButtonType capturar = new ButtonType("Capturar");
		ButtonType atacar = new ButtonType("Atacar");
		ButtonType huir = new ButtonType("Huir" , ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(capturar, atacar, huir);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == capturar) {
			if(partida.getCantidadPokeballs() > 0) {
				capturarPokemon(especieEncontrada);
			} else {
				mostrarAlerta("No tienes pokeballs");
				pasosRandom = (int) (Math.random() * (75 - 5) + 5);
				pasosDados = 0;
			}
		} else if (result.get() == atacar) {
			if(partida.getListadoPokemon().size() > 0) {
				eleccionPokemonCombate();
			} else {
				mostrarAlerta("No tienes pokemons");
				pasosRandom = (int) (Math.random() * (75 - 5) + 5);
				pasosDados = 0;
			}
		} else {
			pasosRandom = (int) (Math.random() * (75 - 5) + 5);
			pasosDados = 0;
		}
	}
	
	/**
	 * Función para mostrar los pokemons que puedes elegir para capturar
	 */
	public void eleccionPokemonCombate() {
		anteriorPanel = explorarPanel;
		actualPanel = eleccionPokemonCombate;
		explorarPanel.setVisible(false);
		eleccionPokemonCombate.setVisible(true);
		selectorPokemonCombateCB.getItems().clear();
		resetSeleccionPokemon();
		for (int i = 0; i < partida.getListadoPokemon().size(); i++) {
			selectorPokemonCombateCB.getItems().add("Especie: " + partida.getPokemon(i).getClass().getSimpleName() + " | Mote: "
					+ partida.getPokemon(i).getNombre() + " | Nivel: " + partida.getPokemon(i).getNivel());
		}
	}
	
	/**
	 * Función para mostrar la información del aliado seleccionado antes del combate
	 */
	public void informacionAliado() {
		int pokemonElegido = selectorPokemonCombateCB.getSelectionModel().getSelectedIndex();
		if (pokemonElegido != -1) {
			Pokemon pokeElegido = partida.getPokemon(pokemonElegido);
			String ataques = "";
			for(int i = 0; i < pokeElegido.getAtaques().length; i++) {
				ataques = ataques + pokeElegido.getAtaques()[i].getNombre() + " | ";
			}
			ataques=ataques.trim();
			nombreAliadoInfoLB.setText("Nombre: " + pokeElegido.getNombre());
			nivelAliadoInfoLB.setText("Nivel: " + pokeElegido.getNivel());
			tipoAliadoInfoLB.setText("Tipo: " + pokeElegido.getTipo());
			ataqueAliadoInfoLB.setText("Ataque: " + pokeElegido.getAtaque() + " | Ataque Especial: " + pokeElegido.getAtaqueEspecial());
			defensaAliadoInfoLB.setText("PS: " + pokeElegido.getPs() + "/" + pokeElegido.getPsOriginal() + " | Defensa: "
					+ pokeElegido.getDefensa() + " | Defensa Especial: " + pokeElegido.getDefensaEspecial());
			ataquesAliadoInfoLB.setText("Ataques: " + ataques);
			capturaAliadoInfoLB.setText("Fecha de captura: " + pokeElegido.getMomentoCaptura());
			File imagenPoke = new File("assets/imagenes/pokemons/" + pokeElegido.getClass().getSimpleName() + ".gif");
			Image img = new Image(imagenPoke.toURI().toString());
			imagenAliadoInfo.setImage(img);
		}
	}
	
	/**
	 * Función para resetear los valores del panel de selecion pokemon
	 */
	public void resetSeleccionPokemon() {
		nombreAliadoInfoLB.setText("Nombre: ");
		nivelAliadoInfoLB.setText("Nivel: ");
		tipoAliadoInfoLB.setText("Tipo: ");
		ataqueAliadoInfoLB.setText("Ataque: ");
		defensaAliadoInfoLB.setText("PS: | Defensa: ");
		ataquesAliadoInfoLB.setText("Ataques: ");
		capturaAliadoInfoLB.setText("Fecha de captura: ");
		File imagenPoke = new File("assets/imagenes/pokeball.png");
		Image img = new Image(imagenPoke.toURI().toString());
		imagenAliadoInfo.setImage(img);
	}
	
	/**
	 * Función para ir al panel del combate una vez elegido el pokemon
	 */
	@FXML public void elegirPokemonCombate() {
		int pokemonElegido = selectorPokemonCombateCB.getSelectionModel().getSelectedIndex();
		Pokemon pokemonAliado = partida.getPokemon(pokemonElegido);
		if(partida.getPokemon(pokemonElegido).getPs() <= 0) {
			mostrarAlerta("Este pokemon no tiene PS!");
		} else {
			mostrarPanelCombate(pokemonAliado);
		}
	}
	
	/**
	 * Función que mostrará el panel de combate listo para usar
	 * @param pokemonAliado 
	 */
	public void mostrarPanelCombate(Pokemon aliado) {
		anteriorPanel = explorarPanel;
		actualPanel = combatePanel;
		eleccionPokemonCombate.setVisible(false);
		combatePanel.setVisible(true);
		enemigo = new Enemigo(especie, nivel);
		
		File imagenFile = new File("assets/imagenes/pokemons/" + aliado.getClass().getSimpleName() + ".gif");
		Image imgAliado = new Image(imagenFile.toURI().toString());
		imgAliadoCombate.setImage(imgAliado);
		
		imagenFile = new File("assets/imagenes/pokemons/" + enemigo.getNombre() + ".gif");
		Image imgEnemigo = new Image(imagenFile.toURI().toString());
		imgEnemigoCombate.setImage(imgEnemigo);
		
		vidaAliadoCombateLB.setText(aliado.getPs() + "/" + aliado.getPsOriginal() + "PS");
		vidaEnemigoCombateLB.setText(enemigo.getPs() + "/" + enemigo.getPsOriginal() + "PS");
		
		Ataque[] ataquesAliado = aliado.getAtaques();
		ataque1Boton.setText(ataquesAliado[0].getNombre() + " | " + ataquesAliado[0].getPp() + "/" + ataquesAliado[0].getPpOriginal());
		ataque2Boton.setText(ataquesAliado[1].getNombre() + " | " + ataquesAliado[1].getPp() + "/" + ataquesAliado[1].getPpOriginal());
		ataque3Boton.setText(ataquesAliado[2].getNombre() + " | " + ataquesAliado[2].getPp() + "/" + ataquesAliado[2].getPpOriginal());
		ataque4Boton.setText(ataquesAliado[3].getNombre() + " | " + ataquesAliado[3].getPp() + "/" + ataquesAliado[3].getPpOriginal());
		
		if(ataquesAliado[3].getNombre().equals("Sin ataque")) {
			ataque4Boton.setDisable(true);
		}
		
		int velocidadAliado = aliado.getVelocidad();
		int velocidadEnemigo = enemigo.getVelocidad();
		
		if(velocidadEnemigo > velocidadAliado) {
			enemigoAtaca(enemigo, aliado);
		} else {
			mensajeCombateLB.setText("Atacas primero");
		}
		
		// daño = 0.01 * danioVariable * ( (0.2*nivelAliado+1) * ataquePokemon * potenciaAtaque / (25 * defensaEnemigo) + 2)
		ataque1Boton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
            	mensajeCombateLB.setText("");
                Ataque ataque = ataquesAliado[0];
                if(ataque.getPp() > 0) {
                	int danio = calcularDanioAliado(ataque, aliado);
	                ataque.setPp(ataque.getPp() - 1);
	                ataque1Boton.setText(ataque.getNombre() + " | " + ataque.getPp() + "/" + ataque.getPpOriginal());
	                enemigo.setPs(enemigo.getPs() - danio);
	                vidaEnemigoCombateLB.setText(enemigo.getPs() + "/" + enemigo.getPsOriginal() + "PS");
	                mensajeCombateLB.setText(mensajeCombateLB.getText() + "¡Has hecho " + danio + " de daño al " + enemigo.getNombre() + " enemigo!");
	                if(enemigo.getPs() <= 0) {
	                	mostrarAlerta("El " + enemigo.getNombre() + " se ha debilitado! Has ganado 300 pokecuartos...");
	                	partida.setPokecuartos(partida.getPokecuartos() + 300);
	                	huirCombate();
	                } else {
	                	enemigoAtaca(enemigo, aliado);
	                }
                } else {
                	mostrarAlerta("No puedes usar este ataque");
                }
            }
        });
		
		ataque2Boton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
            	mensajeCombateLB.setText("");
            	Ataque ataque = ataquesAliado[1];
                if(ataque.getPp() > 0) {
                	int danio = calcularDanioAliado(ataque, aliado);
	                ataque.setPp(ataque.getPp() - 1);
	                ataque2Boton.setText(ataque.getNombre() + " | " + ataque.getPp() + "/" + ataque.getPpOriginal());
	                enemigo.setPs(enemigo.getPs() - danio);
	                vidaEnemigoCombateLB.setText(enemigo.getPs() + "/" + enemigo.getPsOriginal() + "PS");
	                mensajeCombateLB.setText(mensajeCombateLB.getText() + "¡Has hecho " + danio + " de daño al " + enemigo.getNombre() + " enemigo!");
	                if(enemigo.getPs() <= 0) {
	                	mostrarAlerta("El " + enemigo.getNombre() + " se ha debilitado! Has ganado 300 pokecuartos...");
	                	partida.setPokecuartos(partida.getPokecuartos() + 300);
	                	huirCombate();
	                } else {
	                	enemigoAtaca(enemigo, aliado);
	                }
                } else {
                	mostrarAlerta("No puedes usar este ataque");
                }
            }
        });
		
		ataque3Boton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
            	mensajeCombateLB.setText("");
            	Ataque ataque = ataquesAliado[2];
                if(ataque.getPp() > 0) {
                	int danio = calcularDanioAliado(ataque, aliado);
	                ataque.setPp(ataque.getPp() - 1);
	                ataque3Boton.setText(ataque.getNombre() + " | " + ataque.getPp() + "/" + ataque.getPpOriginal());
	                enemigo.setPs(enemigo.getPs() - danio);
	                vidaEnemigoCombateLB.setText(enemigo.getPs() + "/" + enemigo.getPsOriginal() + "PS");
	                mensajeCombateLB.setText(mensajeCombateLB.getText() + "¡Has hecho " + danio + " de daño al " + enemigo.getNombre() + " enemigo!");
	                if(enemigo.getPs() <= 0) {
	                	mostrarAlerta("El " + enemigo.getNombre() + " se ha debilitado! Has ganado 300 pokecuartos...");
	                	partida.setPokecuartos(partida.getPokecuartos() + 300);
	                	huirCombate();
	                } else {
	                	enemigoAtaca(enemigo, aliado);
	                }
                } else {
                	mostrarAlerta("No puedes usar este ataque");
                }
            }
        });
		
		ataque4Boton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
            	mensajeCombateLB.setText("");
            	Ataque ataque = ataquesAliado[3];
                if(ataque.getPp() > 0) {
                	int danio = calcularDanioAliado(ataque, aliado);
	                ataque.setPp(ataque.getPp() - 1);
	                ataque4Boton.setText(ataque.getNombre() + " | " + ataque.getPp() + "/" + ataque.getPpOriginal());
	                enemigo.setPs(enemigo.getPs() - danio);
	                vidaEnemigoCombateLB.setText(enemigo.getPs() + "/" + enemigo.getPsOriginal() + "PS");
	                mensajeCombateLB.setText(mensajeCombateLB.getText() + "¡Has hecho " + danio + " de daño al " + enemigo.getNombre() + " enemigo!");
	                if(enemigo.getPs() <= 0) {
	                	mostrarAlerta("El " + enemigo.getNombre() + " se ha debilitado! Has ganado 300 pokecuartos...");
	                	partida.setPokecuartos(partida.getPokecuartos() + 300);
	                	huirCombate();
	                } else {
	                	enemigoAtaca(enemigo, aliado);
	                }
                } else {
                	mostrarAlerta("No puedes usar este ataque");
                }
            }
        });
		
		curarCombateBoton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				mensajeCombateLB.setText("");
				if (aliado.getPs() < aliado.getPsOriginal()) {
					if (partida.getCantidadPociones() > 0) {
						aliado.setPs(aliado.getPs() + 20);
						partida.eliminarPocion();
						if (aliado.getPs() > aliado.getPsOriginal()) {
							vidaAliadoCombateLB.setText(aliado.getPsOriginal() + "/" + aliado.getPsOriginal() + "PS");
						} else {
							vidaAliadoCombateLB.setText(aliado.getPs() + "/" + aliado.getPsOriginal() + "PS");
						}
						mensajeCombateLB.setText(mensajeCombateLB.getText() + "Has curado a " + aliado.getNombre());
						enemigoAtaca(enemigo, aliado);
					} else {
						mostrarAlerta("¡No tienes pociones!");
					}
				} else {
					mostrarAlerta("¡" + aliado.getNombre() + " tiene la vida al completo!");
				}
			}
		});
	}
	
	/**
	 * Función para calcular el daño que hará el aliado
	 * @param ataque Ataque elegido
	 * @param aliado Pokemon aliado
	 * @return Devolvemos el daño que hará el aliado
	 */
	public int calcularDanioAliado(Ataque ataque, Pokemon aliado) {
		int danio = 0;
		int danioVariable = r.nextInt(101-85) + 85;
		double bonificacion = 1;
		if((aliado.getTipo()).contains(ataque.getTipo())) {
			bonificacion = 1.5;
		}
		if(ataque.getCategoria().equals("Especial")) {
        	danio = (int) (0.01 * danioVariable * bonificacion * ( (0.2 * aliado.getNivel() +1) * aliado.getAtaqueEspecial() * ataque.getPotencia() / (25 * enemigo.getDefensaEspecial()) + 2));
        } else if(ataque.getCategoria().equals("Fisico")) {
        	danio = (int) (0.01 * danioVariable * bonificacion * ( (0.2 * aliado.getNivel() +1) * aliado.getAtaque() * ataque.getPotencia() / (25 * enemigo.getDefensa()) + 2));
        }
		return danio;
	}
	
	/**
	 * Función para que el enemigo ataque
	 * @param enemigo Pokemon enemigo
	 * @param aliado Pokemon aliado
	 */
	public void enemigoAtaca(Enemigo enemigo, Pokemon aliado) {
		int ataqueRandom = (int) (Math.random() * (4 - 1) + 1);
		Ataque ataque = enemigo.getAtaques()[ataqueRandom];
		if (ataque.getNombre().equals("Sin ataque")) {
			enemigoAtaca(enemigo, aliado);
		} else {
			if (enemigo.getAtaques()[ataqueRandom].getPp() > 0) {
				int danio = calcularDanioEnemigo(ataque, aliado);
				aliado.setPs(aliado.getPs() - danio);
				enemigo.getAtaques()[ataqueRandom].setPp(enemigo.getAtaques()[ataqueRandom].getPp() - 1);
				mensajeCombateLB.setText(mensajeCombateLB.getText() + "\nEl " + enemigo.getNombre() + " enemigo ha usado "
						+ enemigo.getAtaques()[ataqueRandom].getNombre() + " y ha hecho " + danio + " de daño a "
						+ aliado.getNombre());
				vidaAliadoCombateLB.setText(aliado.getPs() + "/" + aliado.getPsOriginal() + "PS");
				if (aliado.getPs() <= 0) {
					vidaAliadoCombateLB.setText("0/" + aliado.getPsOriginal() + "PS");
					aliado.setPs(0);
					mostrarAlerta(aliado.getNombre() + " se debilitó! Has perdido!");
					huirCombate();
				}
			}
		}
	}
	
	/**
	 * Función para calcular el daño que hará el enemigo
	 * @param ataque Ataque generado de forma aleatoria
	 * @param aliado Pokemon aliado
	 * @return Devolvemos el daño que hará el enemigo
	 */
	public int calcularDanioEnemigo(Ataque ataque, Pokemon aliado) {
		int danio = 0;
		int danioVariable = r.nextInt(101-85) + 85;
		double bonificacion = 1;
		if((enemigo.getTipo()).contains(ataque.getTipo())) {
			bonificacion = 1.5;
		}
		if (ataque.getCategoria().equals("Especial")) {
			danio = (int) (0.01 * danioVariable * bonificacion * ((0.2 * enemigo.getNivel() + 1) * enemigo.getAtaqueEspecial()
					* ataque.getPotencia() / (25 * aliado.getDefensaEspecial())
					+ 2));
		} else if (ataque.getCategoria().equals("Fisico")) {
			danio = (int) (0.01 * danioVariable * bonificacion * ((0.2 * enemigo.getNivel() + 1) * enemigo.getAtaque()
					* ataque.getPotencia() / (25 * aliado.getDefensa()) + 2));
		}
		return danio;
	}
	
	/**
	 * Función para huir del combate
	 */
	@FXML public void huirCombate() {
		actualPanel.setVisible(false);
		anteriorPanel = principalPanel;
		actualPanel = explorarPanel;
		explorarPanel.setVisible(true);
		pasosRandom = (int) (Math.random() * (75 - 5) + 5);
		pasosDados = 0;
		mensajeCombateLB.setText(""); 
	}
	
	/**
	 * Función para mostrar los datos del pokemon a capturar
	 * @param especieEncontrada Especie generada de forma random
	 */
	public void capturarPokemon(String especieEncontrada) {
		anteriorPanel = explorarPanel;
		actualPanel = capturaPokemon;
		explorarPanel.setVisible(false);
		capturaPokemon.setVisible(true);
		pasosRandom = (int) (Math.random() * (75 - 5) + 5);
		pasosDados = 0;
		File imagenFile = new File("assets/imagenes/pokemons/" + especieEncontrada + ".gif");
		Image img = new Image(imagenFile.toURI().toString());
		imagenPokemonCaptura.setImage(img);
		tituloPokemonCapturaLB.setText(especieEncontrada);
		nivelPokemonCapturaLB.setText("Nivel: " + nivel);
	}

	/**
	 * Función que se ejecutará cuando intentes capturar un Pokemon
	 */
	@FXML public void intentarCaptura() {
		if (partida.getCantidadPokeballs() > 0) {
			mensajePokemonCapturaLB.setVisible(true);
			float porcentajeCaptura = (float) (Math.random() * 1);
			partida.eliminarPokeball();
			if (porcentajeCaptura <= 0.20) {
				capturarBoton.setText("¡Capturado!");
				mensajePokemonCapturaLB.setText("Puedes ponerle un mote a tu nuevo " + especie);
				capturarBoton.setDisable(true);
				motePokemonCapturaFT.setVisible(true);
				guardarPokemonCapturadoBoton.setVisible(true);
			} else {
				capturarBoton.setText("Reintentar");
				mensajePokemonCapturaLB.setText("¡Has fallado! Te quedan " + partida.getCantidadPokeballs() + " pokeballs");
			}
		} else {
			mostrarAlerta("No te quedan pokeballs");
			huirCaptura();
		}		
	}
	
	/**
	 * Función para guardar el pokemon capturado
	 */
	@FXML public void guardarPokemonCapturado() {
		String mote = motePokemonCapturaFT.getText();
		if(mote.equals("")) {
			mote = especie;
		}
		if(especie.equals("Caterpie")) {
			cat = new Caterpie(numPokemon, mote, nivel);
			partida.aniadirPokemon(cat);
		} else if(especie.equals("Lickitung")) {
			lick = new Lickitung(numPokemon, mote, nivel);
			partida.aniadirPokemon(lick);
		} else if(especie.equals("Pidgey")) {
			pid = new Pidgey(numPokemon, mote, nivel);
			partida.aniadirPokemon(pid);
		} else if(especie.equals("Pikachu")) {
			pika = new Pikachu(numPokemon, mote, nivel);
			partida.aniadirPokemon(pika);
		} else if(especie.equals("Rayquaza")) {
			ray = new Rayquaza(numPokemon, mote, nivel);
			partida.aniadirPokemon(ray);
		} else if(especie.equals("Squirtle")) {
			squirt = new Squirtle(numPokemon, mote, nivel);
			partida.aniadirPokemon(squirt);
		}
		numPokemon++;
		partida.setPokecuartos(partida.getPokecuartos() + 350);
		mostrarAlerta("¡Pokemon guardado! Has ganado 350 pokecuartos...");
		huirCaptura();	
	}
	
	
	/**
	 * Función para huir de una captura
	 */
	@FXML public void huirCaptura() {
		mensajePokemonCapturaLB.setVisible(false);
		motePokemonCapturaFT.setVisible(false);
		motePokemonCapturaFT.setText("");
		guardarPokemonCapturadoBoton.setVisible(false);
		capturarBoton.setDisable(false);
		capturarBoton.setText("Capturar");
		anteriorPanel = principalPanel;
		actualPanel = explorarPanel;
		capturaPokemon.setVisible(false);
		explorarPanel.setVisible(true);
		pasosRandom = (int) (Math.random() * (75 - 5) + 5);
		pasosDados = 0;
	}
	
	/**
	 * Función para mostrar una alerta
	 * @param mensaje Mensaje que se mostrará en la alerta
	 */
	public void mostrarAlerta(String mensaje) {
		Alert avisoLiberado = new Alert(AlertType.ERROR);
		Stage stage = (Stage) avisoLiberado.getDialogPane().getScene().getWindow();
		File imagenFile = new File("assets/imagenes/iconoapp.png");
		stage.getIcons().add(new Image(imagenFile.toURI().toString()));
		avisoLiberado.getDialogPane().setGraphic(null);
		avisoLiberado.setTitle(mensaje);
		avisoLiberado.setHeaderText(null);
		avisoLiberado.setContentText(mensaje);
		avisoLiberado.showAndWait();
	}

	/**
	 * Función para volver al inicio del juego desde la creación de partida
	 * o desde la carga de partida
	 */
	@FXML public void volverInicio() {
		anteriorPanel.setVisible(true);
		actualPanel.setVisible(false);
		actualPanel = anteriorPanel;
	}
	
	/**
	 * Función para retroceder entre paneles
	 */
	@FXML public void volver() {
		anteriorPanel.setVisible(true);
		actualPanel.setVisible(false);
		actualPanel = anteriorPanel;
		pokeballsMenuLB.setText(Integer.toString(partida.getCantidadPokeballs()));
		pokecuartosMenuLB.setText(Integer.toString(partida.getPokecuartos()));
	}

	/**
	 * Función para salir del programa
	 */
	@FXML public void salir() {
		System.exit(0);
	}
	
	/**
	 * Función para guardar partida
	 */
	@FXML public void guardarPartida() {
		DbDatos base=getBase();
	    Connection con;
		try {
			con = DriverManager.getConnection(
					"jdbc:mysql://" + base.getIp() + "/" + base.getDb() + "?serverTimezone=UTC", base.getUsername(),
					base.getPassword());
			partida.writeToDB(con);
			mostrarAlerta("¡Partida guardada!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CargarPartidaException e) {
			mostrarAlerta("Ha ocurrido un error con la base de datos");
		}
		
	}
	
	/**
	 * Devuelve los datos de la base de datos
	 * @return DbDatos con los datos de la base datos
	 */
	public DbDatos getBase() {
		Json json = new Json("src/conexion/dbConfig.json");
		DbDatos base = json.getDatosConexion();
		return base;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		inicioPanel.setVisible(true);
		nuevaPartidaPanel.setVisible(false);
		chatOakLB.setVisible(true);
		avanzarChatOak.setVisible(true);
		formularioJugador.setVisible(false);
		generoCB.getItems().addAll("Chico", "Chica", "Otro");
		principalPanel.setVisible(false);
		cargarPanel.setVisible(false);
		informacionJugadorPanel.setVisible(false);
		tiendaPanel.setVisible(false);
		avisoCompraLB.setVisible(false);
		pokemonesPanel.setVisible(false);
		explorarPanel.setVisible(false);
		capturaPokemon.setVisible(false);
		mensajePokemonCapturaLB.setVisible(false);
		motePokemonCapturaFT.setVisible(false);
		guardarPokemonCapturadoBoton.setVisible(false);
		eleccionPokemonCombate.setVisible(false);
		combatePanel.setVisible(false);
		pokecenterPanel.setVisible(false);
		mensajeCuracionLB.setVisible(false);
	}
}
