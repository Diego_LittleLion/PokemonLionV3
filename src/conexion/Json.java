package conexion;

import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;

import modelos.PokemonBasic;

/**
 * Clase para leer de un JSON
 * @author DIEGO LITTLELION
 *
 */
public class Json {
	private Gson gson;
	private FileReader reader = null;
	private PokemonBasic estadisticas;
	private DbDatos datosConexion;

	/**
	 * Función que lee de un JSON los datos de la BBDD o de los Pokemon
	 * @param archivo String con el nombre del archivo JSON
	 */
	public Json(String archivo) {
		String nombre = archivo.substring(0, archivo.lastIndexOf("."));
		try {
			reader = new FileReader(archivo);
			gson = new Gson();
			JsonReader jsonReader = new JsonReader(reader);
			if (nombre.equals("src/conexion/dbConfig")) {
				datosConexion = gson.fromJson(jsonReader, DbDatos.class);
			} else {
				estadisticas = gson.fromJson(jsonReader, PokemonBasic.class);
			}
		} catch (FileNotFoundException es) {
			es.toString();
		} finally {
			try {
				reader.close();
			} catch (IOException ex) {
				System.err.println("Error al cerrar el fichero de configuracion: " + ex.toString());
			} catch (NullPointerException ex) {
				System.err.println("El reader nunca fue creado: " + ex.toString());
			}
		}
	}

	/**
	 * Devuelve las estadisticas de un Pokemon una vez leidas del JSON
	 * @return PokemonBasic con las estadisticas del pokemon
	 */
	public PokemonBasic getEstadisticas() {
		return estadisticas;
	}

	/**
	 * Establece las estadisticas de un Pokemon
	 * @param estadisticas PokemonBasic con las estadisticas de un Pokemon
	 */
	public void setEstadisticas(PokemonBasic estadisticas) {
		this.estadisticas = estadisticas;
	}

	/**
	 * Devuelve los datos de la base de datos
	 * @return DbDatos Con la información de la base de datos
	 */
	public DbDatos getDatosConexion() {
		return datosConexion;
	}

	/**
	 * Establece los datos de la base de datos
	 * @param datosConexion DbDatos con la información de la base de datos
	 */
	public void setDatosConexion(DbDatos datosConexion) {
		this.datosConexion = datosConexion;
	}
	
}
