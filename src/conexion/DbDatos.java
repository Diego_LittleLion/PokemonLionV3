package conexion;

/**
 * Clase para sacar de un JSON los datos de la base de datos
 * @author DIEGO LITTLELION
 */
public class DbDatos {
	private String ip;
	private String db;
	private String username;
	private String password;

	/**
	 * Constructor de la clase DbDatos
	 * @param ip IP de la base de datos
	 * @param db Nombre de la base de datos
	 * @param username Usuario de la base de datos
	 * @param password Contraña del usuario de la base de datos
	 */
	public DbDatos(String ip, String db, String username, String password) {
		this.ip = ip;
		this.db = db;
		this.username = username;
		this.password = password;
	}

	/**
	 * Devuelve la IP de la base de datos
	 * @return String con la ip de la base de datos
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Establece la IP de la base de datos
	 * @param ip IP de la base de datos
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * Devuelve el nombre de la base de datos
	 * @return String con el nombre de la base de datos
	 */
	public String getDb() {
		return db;
	}

	/**
	 * Establece el nombre de la base de datos
	 * @param db Texto con el nombre de la base de datos
	 */
	public void setDb(String db) {
		this.db = db;
	}

	/**
	 * Devuelve el nombre de la base de datos
	 * @return String con el nombre de usuario
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece el usuario de la base de datos
	 * @param username String con el nombre de la base de datos
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Devuelve la contraseña de la base de datos
	 * @return String con la contraseña
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Establece la contraseña de la base de datos
	 * @param password String con la contraseña
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
