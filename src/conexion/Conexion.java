package conexion;

import java.sql.*;

/**
 * Clase para gestionar la conexión con la base de datos
 * 
 * @author luisfernandocasanovacarrasco
 *
 */
public class Conexion {
	private Connection con;
	private Statement query;
	private ResultSet result;
	private DbDatos base;

	/**
	 * Realiza la conexión a la BBDD pasandole un String
	 * @param consulta Query para ejecutar
	 */
	public Conexion(String consulta) {
		try {
			base = getBase();
			this.con = DriverManager.getConnection(
					"jdbc:mysql://" + base.getIp() + "/" + base.getDb() + "?serverTimezone=UTC", base.getUsername(),
					base.getPassword());
			this.query = con.createStatement();
			this.result = query.executeQuery(consulta);
		} catch (SQLException ex) {
			System.out.println("Error en la conexion con " + base.getDb() + ": " + ex.toString());
		}
	}
	
	/**
	 * Realiza la conexión a la BBDD
	 */
	public Conexion() {
		try {
			base=getBase();
			this.con=DriverManager.getConnection("jdbc:mysql://" + base.getIp() + "/" + base.getDb() + "?serverTimezone=UTC", base.getUsername(),
					base.getPassword());
			this.query=con.createStatement();
		} catch (SQLException ex) {
			System.out.println("Error en la conexion con " + base.getDb() + ": " + ex.toString());		
		}
	}

	/**
	 * Devuelve el resultset
	 * @return ResultSet
	 */
	public ResultSet getResult() {
		return this.result;
	}

	/**
	 * Devuelve el resultado de una query
	 * @param consulta - query para ejecutar
	 * @return ResultSet
	 */
	public ResultSet getResult(String consulta) {
		try {
			result = query.executeQuery(consulta);
		} catch (SQLException ex) {
			ex.printStackTrace();

		}
		return this.result;
	}

	/**
	 * Cierra la conexión a la base de datos
	 */
	public void close() {
		try {
			result.close();
			query.close();
			con.close();
		} catch (SQLException ex) {
			ex.toString();
		}
	}
	
	/**
	 * Devuelve los datos de conexion leidas de un Json
	 * @return DbDatos
	 */
	public DbDatos getBase() {
		Json json = new Json("src/conexion/dbConfig.json");
		DbDatos base = json.getDatosConexion();
		return base;
	}
}
