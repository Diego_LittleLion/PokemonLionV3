drop database if exists PokemonLionV3;
create database PokemonLionV3;
use PokemonLionV3;

create table Partidas(
		id int primary key auto_increment,
        nombre varchar(250),
        fechaNacimiento varchar(250),
        genero varchar(250),
        pokecuartos int
);

create table Pokemons(
	idPartida int references Partidas(id),
    idPokemon int,
    especie varchar(250),
    nombre varchar(250),
    nivel int,
    tipo varchar(250),
    ps int,
    ataque int,
    defensa int,
    ataqueEspecial int,
    defensaEspecial int,
    velocidad int,
    primary key(idPartida, idPokemon)
);

create table Objetos(
	idPartida int references Partidas(id),
    idObjeto int,
    nombre varchar(250),
    cantidad int,
    primary key(idPartida, idObjeto)
);

INSERT INTO Partidas(nombre, fechaNacimiento, genero, pokecuartos) VALUES('puppet', '12/05/2019', 'Otro', 200);
INSERT INTO Objetos(idPartida, idObjeto, nombre, cantidad) VALUES(1, 1, 'pokeball', 10);
INSERT INTO Objetos(idPartida, idObjeto, nombre, cantidad) VALUES(1, 2, 'pocion', 5);
INSERT INTO Pokemons(idPartida, idPokemon, especie, nombre, nivel, tipo, ps, ataque, defensa, ataqueEspecial, defensaEspecial, velocidad) VALUES('1', '0', 'Rayquaza', 'Snake', 65,	'Volador Dragón', 320, 305, 185, 305, 185, 195);