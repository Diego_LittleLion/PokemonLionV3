POKEMON LION GUI
================

Descripción del proyecto
------------------------

Juego basado en el mundo de Pokemon. En este juego deberás capturar Pokemons (como en todos los juegos),
podrás administrar tus capturas (liberar a un Pokemon o consultar sus detalles), comprar más pokeballs para seguir capturando....


Instalación
-----------

Está escrito en Java y la interfaz está hecha con JavaFX, el IDE usado es Eclipse aunque lo podéis abrir en netbeans importando el proyecto.
Antes de compilar tendréis que ejecutar el fichero sql que se encuentra en la carpeta assets, una vez hecho esto ya podéis compilar el proyecto.


Controles
---------
El personaje se maneja con las flechas del teclado


Copyright
---------
Todas las imagenes y sonidos pertenecen a sus respectivos creadores...


Autores
-------

DIEGO LITTLELION
Twitter: https://twitter.com/LittleFyahLion

LUIS FERNANDO CASANOVA